import java.util.Arrays;

public class Main {

    public static int findMinMulti(int[] arr, int n) {
        int min = Integer.MAX_VALUE;
        for(int i = 0; i < n - 1; i++) {
            for(int j = i + 1; j < n; j++) {
                if (arr[i] * arr[j] < min)
                    min = arr[i] * arr[j];
            }
        }

        return min;
    }

    public static int findMinMultiSort(int[] arr, int n) {
        Arrays.sort(arr, 0, n);
        return arr[0] * arr[1];
    }

    public static void main(String[] args) {
        int[] arr = {3, 4, 9, 6, 2, 4};
        System.out.println(findMinMulti(arr, arr.length));
        System.out.println(findMinMultiSort(arr, arr.length));

    }
}
